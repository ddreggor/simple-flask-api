import json

def test_assert():
    assert True

def test_index(client,db):
    res = client.get('/features')
    assert res.status_code == 200
    expected = db
    #expected = {'features': ['features', 'available_packages', 'features', 'available_packages', 'features', 'available_packages']}
    assert expected == json.loads(res.get_data(as_text=True))
