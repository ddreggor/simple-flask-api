
from flask import Flask, jsonify, request, make_response
import simplejson as json
import os
import subprocess
import configparser

def create_app(app_conf):
    curr_path = os.path.dirname(os.path.realpath(__file__))

    app = Flask(__name__)
    app.config['JSONIFY_PRETTYPRINT_REGULAR'] = False
    db = "{}/{}/{}".format(curr_path, app_conf['database_folder'], app_conf["database_file"])
    if not os.path.exists(db):
         db = "{}/database/{}".format(curr_path, app_conf["database_file"])
    print(" * Using Database Location: '{}'".format(db))

    with open(db, "r") as jsf:
        feature_list = json.load(jsf)

    @app.route('/', methods=['GET'])
    def index():
        ''' Ask item to look elsewhere '''
        return "Noting useful to see here"

    @app.route('/features', methods=['GET'])
    def show_features():
        ''' Displays all the features '''

        resp = make_response(json.dumps(feature_list, sort_keys=True, indent=4))
        resp.headers['Content-Type']="application/json"
        return resp

    @app.route('/features/<item>', methods=['GET'])
    def item_feature_list(item):
        ''' Returns an item oriented list '''

        if item not in feature_list:
            return "No list found"

        return jsonify(feature_list[item])

    return app


if __name__ == '__main__':
    config = configparser.ConfigParser()
    config.read('conf/app.conf')
    app_conf = config["catalog_detail"]
    app = create_app(app_conf)
    app.run('0.0.0.0',port=int(app_conf["port"]), debug=True)
