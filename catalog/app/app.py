from flask import Flask, jsonify, make_response
import requests
import os
import simplejson as json
import subprocess
import configparser

def create_app(app_conf, detail_api_url):
    app = Flask(__name__)
    app.config['JSONIFY_PRETTYPRINT_REGULAR'] = False

    curr_path = os.path.dirname(os.path.realpath(__file__))

    db = "{}/{}/{}".format(curr_path, app_conf['database_folder'], app_conf["database_file"])
    if not os.path.exists(db):
         db = "{}/database/{}".format(curr_path, app_conf["database_file"])
    print(" * Using Database Location: '{}'".format(db))
    print(" * Using Detail api URL: '{}'".format(detail_api_url))

    with open(db, "r") as f:
        mycatalog = json.load(f)

    @app.route("/", methods=['GET'])
    def index():
        ''' Ask item to look elsewhere '''
        return "Noting useful to see here"

    @app.route('/catalog', methods=['GET'])
    def catalog():
        ''' Returns the list of catalog items '''

        resp = make_response(json.dumps(mycatalog, sort_keys=True, indent=4))
        resp.headers['Content-Type']="application/json"
        return resp

    @app.route('/catalog/<item>', methods=['GET'])
    def item_data(item):
        ''' Returns info about a specific item '''

        if item not in mycatalog:
            return "Not found"

        return jsonify(mycatalog[item])

    @app.route('/catalog/<item>/features', methods=['GET'])
    def features(item):
        ''' Get item features '''

        try:
            # FOR OCP we use <app name>.<namespace>:<port> (catalog-detail.simple-flask-api:8090) to get svc
            req = requests.get("{}/features/{}".format(detail_api_url, item))
        except requests.exceptions.ConnectionError:
            return "Service unavailable"
        return req.text
    return app

if __name__ == '__main__':
    config = configparser.ConfigParser()
    config.read('conf/app.conf')
    app_conf = config["catalog"]
    detail_api_url = config["catalog_detail"]["api_url"]
    app = create_app(app_conf, detail_api_url)
    app.run('0.0.0.0',port=int(app_conf["port"]), debug=True)
