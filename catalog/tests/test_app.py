import json


def test_assert():
    assert True

def test_index(client):
    res = client.get('/')
    assert res.status_code == 200

def test_catalog(client, db):
    # This tests that we are correctly pulling the data
    res = client.get('/catalog')
    assert res.status_code == 200
    expected = db
    assert expected == json.loads(res.get_data(as_text=True))
