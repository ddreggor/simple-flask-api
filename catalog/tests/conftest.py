import pytest
import os
import json
import configparser
from app import app as myapp

config = configparser.ConfigParser()
config.read('app/conf/app.conf')
app_conf = config["catalog"]
detail_api_url = config["catalog_detail"]["api_url"]

@pytest.fixture
def client():
    app = myapp.create_app(app_conf, detail_api_url)
    return app.test_client()

@pytest.fixture
def db():
    curr_path = os.path.dirname(os.path.dirname(os.path.realpath(__file__))) + "/app"
    db = "{}/{}/{}".format(curr_path, app_conf['database_folder'], app_conf["database_file"])
    if not os.path.exists(db):
         db = "{}/database/{}".format(curr_path, app_conf["database_file"])
    with open(db, "r") as f:
        mycatalog = json.load(f)
    return mycatalog
