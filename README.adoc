= Simple Flask API
:toc:

A small demo project to show how to build images and deploy a small set of flask api microservices

== The hosting service
.Disclaimer
This project mocks up the backend catalog and catalog detail microservices for a small hosting company. Obviously this is not intended to be a real application or representative of real endpoints or data, only that it *has* both (valid endpoints and some data) to show.

=== Catalog

This api microservice is simply a demonstration of routes that return catalog items defined in a database (`catalog.json`)

=== Catalog detail

This api microservice is simply a demonstration of routes that return catalog item features defined in a database (`catalog_detail.json`)

== Building the images

Building the images that will be used later in a deployment to podman, kubernetes, or OCP/OKD should be straight forward...

----
cd catalog
buildah bud --format=docker --tag <REPO>/simple_flask_api/catalog:latest .
cd ../catalog_detail
buildah bud --format=docker --tag <REPO>/simple_flask_api/catalog_detail:latest .
cd ..
----

== Using the images

=== Locally (podman)

include::podman-build/podman.adoc[leveloffset=+3]

=== OpenShift (manual deployment)

include::manual-build/manual.adoc[leveloffset=+3]

=== OpenShift (ansible deployment)

include::ansible-build/ansible.adoc[leveloffset=+3]

=== Testing

Once deployed you should be able to use either localhost (for podman deploy) or the route provided by OCP/OKD to test the endpoints

==== The catalog app

This app gives us access to the catalog items and their information

.Catalog endpoints
[width="75%",cols="<s,e",frame="topbot",options="header"]
|==========================
|Endpoint|Description
| /catalog
|Get a list of items in the catalog

|/catalog/<item>
|Get information about this catalog item

*You must know the catalog item*
|==========================



==== The catalog-detail app

This app gives us access to the catalog items detailed features

.Catalog detail
[width="75%",cols="<s,e",frame="topbot",options="header"]
|==========================
|Endpoint|Description
| /features
|Get a list of features of all catalog items

|/features/<item>
|Get features about one catalog item

*You must know the catalog item*
|==========================

==== The communication between the two

There is a special route in the catalog app that calls the catalog-detail app to tie catalog items to it's corresponding features.


.Cross communication
[width="75%",cols="<s,e",frame="topbot",options="header"]
|==========================
|Endpoint|Description

|/catalog/<item>/features
|Get features about one catalog item

*You must know the catalog item*
|==========================

==== Example Test commands
Test the endpoints using your browser or curl
----
curl http://localhost/catalog
curl http://localhost/catalog/hosting_enterprise
curl http://localhost/features
curl http://localhost/features/hosting_soho
curl http://localhost/catalog/hosting_soho/features
----


The last two are actually the same results because the very last one calls the catalog-detail app using the url `features/hosting_soho`!

.The catalog app uses this code to do that:
----
@app.route('/catalog/<item>/features', methods=['GET'])
def features(item):
    ''' Get item features '''

    try:
        # FOR OCP we use <app name>.<namespace>:<port> (catalog-detail.simple-flask-api:8090) to get svc
        req = requests.get("{}/features/{}".format(detail_api_url, item))
    except requests.exceptions.ConnectionError:
        return "Service unavailable"
----
